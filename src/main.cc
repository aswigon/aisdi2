#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "Graph.h"
#include "GraphReader.h"
#include "GraphSolver.h"

namespace
{
    using args_t = const std::vector<std::string> &;

    void main_stage2(args_t args, args_t files)
    {
        if (!args.empty())
            throw std::runtime_error("Arguments not supported yet");
        else if (files.empty())
            throw std::runtime_error("No file specified");

        for (auto &f : files)
        {
            bool is_stdin = (f == "-");
            std::string name = (is_stdin ? "<stdin>" : f);

            aisdi::Graph g;

            if (is_stdin)
            {
                aisdi::GraphReader::read(g, std::cin);
            }
            else
            {
                std::ifstream ifs(name);
                if (!ifs.is_open())
                    throw std::runtime_error("Cannot open file");
                aisdi::GraphReader::read(g, ifs);
            }

            if (files.size() > 1)
                std::cout << name << ":\n";

            for (auto &e : aisdi::GraphSolver::bridges(g))
            {
                std::cout << e.first << " " << e.second << "\n";
            }
        }
    }
}

int main(int argc, char **argv)
{
    // Collect arguments
    std::vector<std::string> args;
    std::vector<std::string> files;

    for (int i = 1; i < argc; ++i)
    {
        const char *arg = argv[i];

        if (arg[0] == '-')
        {
            if (arg[1] == '-') // a GNU-style long option
            {
                args.emplace_back(arg + 2);
            }
            else if (arg[1] == '\0') // special file name for stdin
            {
                files.emplace_back(arg);
            }
            else // a POSIX-style short option
            {
                // TODO: Handle options with parameters (e.g. -o file)
                args.emplace_back(arg + 1);
            }
        }
        else // not a parameter
        {
            files.emplace_back(arg);
        }
    }

    // Default to stdin
    if (files.empty())
        files.emplace_back("-");

    try
    {
        main_stage2(args, files);
    }
    catch (const std::exception &e)
    {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    }

    return 0;
}
