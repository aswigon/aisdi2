#ifndef AISDI_GRAPHREADER_H
#define AISDI_GRAPHREADER_H

#include "Graph.h"

namespace aisdi
{
    class GraphReader
    {
    public:
        GraphReader() = delete;

        static void read(aisdi::Graph &g, std::istream &is);
    };
}

#endif // AISDI_GRAPHREADER_H
