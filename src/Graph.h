#ifndef AISDI_GRAPH_H
#define AISDI_GRAPH_H

#include <algorithm>
#include <cstddef>
#include <stdexcept>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

namespace aisdi
{
    class Graph
    {
    public:
        using edge_type = std::pair<int, int>;
        using vertex_type = int;
        using graph_type = std::unordered_map<vertex_type, std::unordered_set<vertex_type>>;
        using iterator = graph_type::iterator;
        using const_iterator = graph_type::const_iterator;

        Graph() = default;
        Graph(const Graph &) = default;
        Graph(Graph &&) noexcept = default;
        ~Graph() noexcept = default;

        Graph &operator=(const Graph &) = default;
        Graph &operator=(Graph &&) noexcept = default;

        bool operator==(const Graph &g) { return (_graph == g._graph); }
        bool operator!=(const Graph &g) { return !(*this == g); }

        /**
         * Checks if a vertex belongs to the graph.
         *
         * @param v vertex to be queried
         * @retval true if `v` belongs to the graph
         * @retval false otherwise
         */
        bool contains(const vertex_type &v) const { return (_graph.find(v) != _graph.end()); }

        /**
         * Checks if an edge belongs to the graph.
         *
         * @param e edge to be queried
         * @retval true if `e` belongs to the graph
         * @retval false otherwise
         */
        bool contains(const edge_type &e) const;

        /**
         * Returns the number of edges.
         *
         * @return number of edges
         */
        std::size_t edge_count() const;

        /**
         * Returns the number of vertices.
         *
         * @return number of vertices
         */
        std::size_t vertex_count() const { return _graph.size(); }

        /**
         * Returns the neighbors of a given vertex.
         *
         * @return neighbors
         */
        std::vector<vertex_type> neighbors(const vertex_type &) const;

        /**
         * Creates a new vertex with no neighbors.
         *
         * @param v vertex to be created
         * @retval true if `v` was not a vertex before
         * @retval false otherwise
         */
        bool add(const vertex_type &v);

        /**
         * Creates a new edge.
         *
         * @param e edge to be created
         * @retval true if `e` was not an edge before
         * @retval false otherwise
         * @throw std::invalid_argument if vertices to be connected do not exist
         * @throw std::invalid_argument if both ends are the same
         */
        bool connect(const edge_type &e);

        /**
         * Removes an edge.
         *
         * @param e edge to be removed
         * @retval true if `e` was an edge before
         * @retval false otherwise
         */
        bool disconnect(const edge_type &e);

        /**
         * Removes a vertex.
         *
         * @param v vertex to be removed
         * @retval true if `v` was a vertex before
         * @retval false otherwise
         */
        bool remove(const vertex_type &v);

        iterator begin() { return _graph.begin(); }
        const_iterator begin() const { return _graph.cbegin(); }
        iterator end() { return _graph.end(); }
        const_iterator end() const { return _graph.cend(); }

    private:
        graph_type _graph;
    };
}

#endif // AISDI_GRAPH_H
