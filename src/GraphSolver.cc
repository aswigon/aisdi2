#include <algorithm>
#include <queue>
#include <unordered_set>
#include <vector>

#include "Graph.h"
#include "GraphSolver.h"

namespace
{
    bool is_connected(const aisdi::Graph &g, const aisdi::Graph::edge_type &e)
    {
        std::queue<int> to_visit;
        std::vector<int> visited(g.vertex_count());

        int current = 0;

        while (current == e.first || current == e.second)
            ++current;

        if (e.first >= 0) visited[e.first] = 1;
        if (e.second >= 0) visited[e.second] = 1;

        while (current != -1)
        {
            auto nb = g.neighbors(current);
            visited[current] = 1;

            for (auto &v : nb)
            {
                if (visited[v] > 0)
                    continue;

                visited[v] = 1;
                to_visit.push(visited[v]);
            }

            if (!to_visit.empty())
            {
                current = to_visit.front();
                to_visit.pop();
            }
            else
            {
                current = -1;
            }
        }

        for (auto &v : g)
        {
            if (visited[v.first] == 0)
                return false;
        }

        return true;
    }
}

std::vector<aisdi::Graph::edge_type> aisdi::GraphSolver::bridges(const Graph &g)
{
    std::vector<Graph::edge_type> bridges;

    for (auto &a : g)
    {
        for (auto &b : g.neighbors(a.first))
        {
            if (a.first < b && !is_connected(g, {a.first, b}))
                bridges.push_back({a.first, b});
        }
    }

    return bridges;
}
