#include "Graph.h"

bool aisdi::Graph::contains(const edge_type &e) const
{
    auto it = _graph.find(e.first);
    if (it == _graph.end())
        return false;
    else if (it->second.find(e.second) == it->second.end())
        return false;
    else
        return true;
}

std::size_t aisdi::Graph::edge_count() const
{
    std::size_t sz = 0;

    for (auto &v : _graph)
        sz += v.second.size();

    if (sz % 2 != 0)
        throw std::logic_error("Inconsistent state");

    return (sz / 2);
}

std::vector<aisdi::Graph::vertex_type> aisdi::Graph::neighbors(const vertex_type &v) const
{
    std::vector<vertex_type> nb;

    auto it = _graph.find(v);

    if (it != _graph.end())
        for (auto &a : it->second)
            nb.push_back(a);

    return nb;
}

bool aisdi::Graph::add(const vertex_type &v)
{
    std::pair<vertex_type, std::unordered_set<vertex_type>> p{v, std::unordered_set<vertex_type>{}};
    return _graph.insert(p).second;
}

bool aisdi::Graph::connect(const edge_type &e)
{
    if (e.first == e.second)
        throw std::invalid_argument("The edge is a loop");
    else if (!contains(e.first) || !contains(e.second))
        throw std::invalid_argument("Required vertices do not exist");

    bool b1 = _graph[e.first].insert(e.second).second;
    bool b2 = _graph[e.second].insert(e.first).second;

    if (b1 != b2)
        throw std::logic_error("Inconsistent state");

    return b1;
}

bool aisdi::Graph::disconnect(const edge_type &e)
{
    if (!contains(e))
        return false;

    bool b1 = (_graph[e.first].erase(e.second) != 0);
    bool b2 = (_graph[e.second].erase(e.first) != 0);

    if (b1 != b2)
        throw std::logic_error("Inconsistent state");

    return b1;
}

bool aisdi::Graph::remove(const vertex_type &v)
{
    for (auto &a : _graph)
    {
        a.second.erase(v);
    }

    return (_graph.erase(v) != 0);
}
