#ifndef AISDI_GRAPHSOLVER_H
#define AISDI_GRAPHSOLVER_H

#include <utility>
#include <vector>

#include "Graph.h"

namespace aisdi
{
    class GraphSolver
    {
    public:
        GraphSolver() = delete;

        static std::vector<aisdi::Graph::edge_type> bridges(const Graph &g);
    };
}

#endif // AISDI_GRAPHSOLVER_H
