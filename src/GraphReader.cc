#include <iostream>
#include <regex>
#include <string>

#include "Graph.h"
#include "GraphReader.h"

void aisdi::GraphReader::read(aisdi::Graph &g, std::istream &is)
{
    std::string line;
    std::getline(is, line);
    std::smatch match;
    int count;

    if (std::regex_match(line, match, std::regex(R"(\s*([0-9]+)\s*)")))
        count = std::stoi(match[1].str());
    else
        throw std::runtime_error("Syntax error");

    for (int i = 0; i < count; ++i)
        g.add(i);

    while (true)
    {
        std::getline(is, line);

        if (is.eof())
            break;
        else if (is.fail())
            throw std::runtime_error("I/O error");

        if (std::regex_match(line, match, std::regex(R"(\s*([0-9]+)\s*([0-9]+)\s*)")))
            g.connect({std::stoi(match[1].str()), std::stoi(match[2].str())});
        else
            throw std::runtime_error("Syntax error");
    }
}
