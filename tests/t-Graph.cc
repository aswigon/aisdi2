#include "catch.hpp"

#include <sstream>

#include <Graph.h>
#include <GraphReader.h>

TEST_CASE("graph empty")
{
    aisdi::Graph g;

    REQUIRE(g.edge_count() == 0);
    REQUIRE(g.vertex_count() == 0);
    REQUIRE_FALSE(g.remove(1));
    REQUIRE_FALSE(g.disconnect({1, 2}));
}

TEST_CASE("graph v1e0")
{
    aisdi::Graph g;

    REQUIRE(g.add(1));
    REQUIRE_FALSE(g.add(1));
    REQUIRE_THROWS(g.connect({1, 1}));
    REQUIRE_THROWS(g.connect({0, 1}));
    REQUIRE(g.edge_count() == 0);
    REQUIRE(g.vertex_count() == 1);
    REQUIRE(g.contains(1));

    REQUIRE_FALSE(g.contains(0));
    REQUIRE_FALSE(g.contains({0, 1}));
    REQUIRE(g == g);

    REQUIRE_FALSE(g.disconnect({1, 1}));
    REQUIRE(g.remove(1));
    REQUIRE_FALSE(g.remove(1));
    REQUIRE(g.edge_count() == 0);
    REQUIRE(g.vertex_count() == 0);
}

TEST_CASE("graph v2e1")
{
    aisdi::Graph g;

    REQUIRE(g.add(1));
    REQUIRE(g.add(7));
    REQUIRE(g.vertex_count() == 2);
    REQUIRE(g.connect({7, 1}));
    REQUIRE(g.contains({1, 7}));
    REQUIRE_FALSE(g.connect({1, 7}));
    REQUIRE_FALSE(g.connect({7, 1}));
    REQUIRE(g.edge_count() == 1);

    REQUIRE(g.remove(1));
    REQUIRE(g.edge_count() == 0);
    REQUIRE(g.vertex_count() == 1);
    REQUIRE_FALSE(g.disconnect({1, 7}));
}

TEST_CASE("graph v7e5")
{
    aisdi::Graph g;

    for (int i : {1, 5, 3, 2, 4, 7, 6})
        g.add(i);

    //{{5, 1}, {4, 2}, {2, 6}, {2, 7}, {7, 6}})
    g.connect({5, 1});
    g.connect({4, 2});
    g.connect({2, 6});
    g.connect({2, 7});
    g.connect({7, 6});

    REQUIRE_FALSE(g.connect({6, 2}));
    REQUIRE(g.vertex_count() == 7);
    REQUIRE(g.edge_count() == 5);

    REQUIRE(g.contains(2));
    REQUIRE(g.contains({1, 5}));
    REQUIRE(g.contains({7, 2}));
    REQUIRE_FALSE(g.contains(8));
    REQUIRE_FALSE(g.contains({4, 6}));

    REQUIRE(g.remove(2));
    REQUIRE(g.vertex_count() == 6);
    REQUIRE(g.edge_count() == 2);
    REQUIRE_FALSE(g.contains({2, 6}));
}

TEST_CASE("graph read")
{
    aisdi::Graph g;

    std::string s = R"(4
        0 1
        1 2
        2 3
        3 0
        0 2
        )";

    std::istringstream is(s);
    aisdi::GraphReader::read(g, is);
    REQUIRE(g.vertex_count() == 4);
    REQUIRE(g.edge_count() == 5);
}
